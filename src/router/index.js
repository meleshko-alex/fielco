import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '@/views/Home';
import TestPage from '@/views/TestPage';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/test',
    name: 'TestPage',
    component: TestPage
  },
];

const router = new VueRouter({
  routes
});

export default router;
