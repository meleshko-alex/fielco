export default {
  props:{
    rulesProp:{
      type: Array,
      default: ()=>[],
    }
  },
  data(){
    return{
      rules: [],
      errMessage:'',
    };
  },
  methods:{
    checkRules(value) {
      if (!this.rules) return;
      for (let rule of this.rules) {
        const message = rule(value);
        if (typeof message === 'string') {
          this.errMessage = message;
          return;
        }
      }
      this.errMessage = null;
    },
  },
  created() {
    this.rules=this.$props.rulesProp;
  }
};
